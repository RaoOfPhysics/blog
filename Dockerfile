# This Dockerfile builds an environment for
# Achintya Rao's blog

# The base image comes from the good people at rocker-org
FROM rocker/geospatial:4.1
ADD . /home/rstudio
WORKDIR /home/rstudio
RUN sudo apt remove hugo -y
RUN Rscript -e "source('.Rprofile')"
RUN Rscript -e "install.packages('here')"
RUN Rscript -e "install.packages('cowsay')"
RUN Rscript -e "install.packages('renv')"
RUN Rscript -e "renv::consent(provided = TRUE)"
RUN Rscript -e "renv::restore()"

